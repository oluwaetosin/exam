import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {
  result: number;
  constructor() {
    this.result = 0;
  }

  ngOnInit(): void {
  }
  add(number1:number , number2: number){
    this.result = number1 + number2;
  }
}
