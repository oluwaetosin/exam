import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TitleComponent } from './title/title.component';
import { NameComponent } from './name/name.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { VotersComponent } from './voters/voters.component';
import { VotersCardComponent } from './voters-card/voters-card.component';

@NgModule({
  declarations: [
    AppComponent,
    TitleComponent,
    NameComponent,
    CalculatorComponent,
    VotersComponent,
    VotersCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
