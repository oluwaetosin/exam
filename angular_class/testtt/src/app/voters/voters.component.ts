import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-voters',
  templateUrl: './voters.component.html',
  styleUrls: ['./voters.component.scss']
})
export class VotersComponent implements OnInit {
  buhariCounts: number;
  ambaliCounts: number;
  ojukwuCounts: number;
  candidates: Array<string>;

  constructor() {
    this.buhariCounts = 0;
    this.ambaliCounts = 0;
    this.ojukwuCounts = 0;
    this.candidates = ['Buhari','Ambali','Ojukwu'];
  }



  ngOnInit(): void {
  }

  voteBuhari(decision: boolean){
    if(decision){
      this.buhariCounts = this.buhariCounts + 1;
    } else{
      this.buhariCounts = this.buhariCounts - 1;
    }
  }

  voteAmbali(decision: boolean){
    if(decision){
      this.ambaliCounts = this.ambaliCounts + 1;
    } else{
      this.ambaliCounts = this.ambaliCounts - 1;
    }
  }


  voteOjukwu(decision: boolean){
    if(decision){
      this.ojukwuCounts = this.ojukwuCounts + 1;
    } else{
      this.ojukwuCounts = this.ojukwuCounts - 1;
    }
  }

}
