import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent implements OnInit {

title: string = "Title from new component";
 private clickTracker: number = 0;
  constructor() { }

  ngOnInit(): void {
  }

  changeTitle(){
    this.clickTracker++;
    this.title = `I just updated the title ${this.clickTracker}`;

  }

}
