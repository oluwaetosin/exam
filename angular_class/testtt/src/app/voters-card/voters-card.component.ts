import { EventEmitter } from '@angular/core';
import { Component, Input, OnInit, Output } from '@angular/core';


@Component({
  selector: 'app-voters-card',
  templateUrl: './voters-card.component.html',
  styleUrls: ['./voters-card.component.scss']
})
export class VotersCardComponent implements OnInit {
  @Input('candidate') name : string;
  @Output() voteBuhari: EventEmitter<boolean> = new EventEmitter();
  @Output() voteAmbali: EventEmitter<boolean> = new EventEmitter();
  @Output() voteOjukwu: EventEmitter<boolean> = new EventEmitter();
  voteCounts: number;
  constructor() {
    this.name = '';
    this.voteCounts = 0;
  }

  ngOnInit(): void {
    // setInterval(()=>{
    //   this.voteCounts += 4;
    // },3000);
  }
  voteYes(){
    this.voteCounts = this.voteCounts + 1;
    switch(this.name.toLowerCase()){
      case 'buhari':
        this.voteBuhari.emit(true);
        break;
      case 'ojukwu':
      this.voteOjukwu.emit(true);
      break;
      case 'ambali':
      this.voteAmbali.emit(true);
      break;

    }
  }
  voteNo(){
    this.voteCounts = this.voteCounts - 1;
    switch(this.name.toLowerCase()){
      case 'buhari':
        this.voteBuhari.emit(false);
        break;
      case 'ojukwu':
      this.voteOjukwu.emit(false);
      break;
      case 'ambali':
      this.voteAmbali.emit(false);
      break;

    }
  }

}
