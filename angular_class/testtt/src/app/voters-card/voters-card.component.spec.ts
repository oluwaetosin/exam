import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VotersCardComponent } from './voters-card.component';

describe('VotersCardComponent', () => {
  let component: VotersCardComponent;
  let fixture: ComponentFixture<VotersCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VotersCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VotersCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
